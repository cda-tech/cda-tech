<?php

date_default_timezone_set('America/Winnipeg');

require_once __DIR__ . '/../../vendor/autoload.php';

$app = require_once __DIR__.'/../../bootstrap/app.php';

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

use App\Providers\AppServiceProvider;
use App\Services\IIcecat as IIcecatService;

$appServiceProvider = new AppServiceProvider($app);
$appServiceProvider->register();

$icecatService = app(IIcecatService::class);

$validGTINs = [];
$forbiddenGTINs = [];
$notFoundGTINs = [];
$otherGTINs = [];

$file = fopen($argv[1], 'r');
if ($file) {
    while(($line = fgets($file)) !== false) {
        try {
            $icecatService->get($line);
            $validGTINs[] = $line;
        } catch (\Exception $e) {
            if ($e->getCode() === 403) {
                $forbiddenGTINs[] = $line;
            } else if ($e->getCode() === 404) {
                $notFoundGTINs[] = $line;
            } else {
                $otherGTINs[] = $line;
            }
        }
    }

    fclose($file);
}

$outputFile = fopen('resources/filtered-GTINs.txt', 'w');

fwrite($outputFile, "Valid GTINs: " . count($validGTINs) . "\n");
foreach ($validGTINs as $gtin) {
    fwrite($outputFile, $gtin);
}

fwrite($outputFile, "\n\nForbidden GTINs: " . count($forbiddenGTINs) . "\n");
foreach ($forbiddenGTINs as $gtin) {
    fwrite($outputFile, $gtin);
}

fwrite($outputFile, "\n\nNot Found GTINs: " . count($notFoundGTINs) . "\n");
foreach ($notFoundGTINs as $gtin) {
    fwrite($outputFile, $gtin);
}

fwrite($outputFile, "\n\nOther GTINs: " . count($otherGTINs) . "\n");
foreach ($otherGTINs as $gtin) {
    fwrite($outputFile, $gtin);
}

fclose($outputFile);
