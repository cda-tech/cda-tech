<?php

namespace App\IcecatClient\Services;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use SimpleXMLElement;

class Client
{
    private static $DEFAULT_LANGUAGE = 'en';

    private static $DEFAULT_FILE_TYPE = 'json';

    private static $DEFAULT_USERNAME = 'JamesADV';

    protected $rootPath = 'https://live.icecat.biz/api';

    protected $guzzleClient;

    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    public function post($path, $params = [], $body)
    {
        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $headers = [];

        $request = new Request('POST', $uri, $headers, $body);

        return $this->sendRequestToIcecat($request);
    }

    public function put($path, $params = [], $body)
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('PUT', $uri, $headers, $body);

        return $this->sendRequestToIcecat($request);
    }

    public function delete($path, $params = [])
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('DELETE', $uri, $headers);

        return $this->sendRequestToIcecat($request);
    }

    public function get($path = '', $params = [])
    {
        $headers = [];

        $params['lang'] = isset($params['lang']) ? $params['lang'] : $this::$DEFAULT_LANGUAGE;
        $params['type'] = isset($params['type']) ? $params['type'] : $this::$DEFAULT_FILE_TYPE;
        $params['UserName'] = isset($params['UserName']) ? $params['UserName'] : $this::$DEFAULT_USERNAME;

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('GET', $uri, $headers);

        return $this->sendRequestToIcecat($request);
    }

    public function head($path, $params = [])
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('HEAD', $uri, $headers);

        return $this->sendRequestToIcecat($request);
    }

    private function sendRequestToIcecat(Request $request)
    {
        $response = $this->guzzleClient->send($request);

        return json_decode($response->getBody()->getContents(), true);
    }
}
