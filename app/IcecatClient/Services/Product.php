<?php

namespace App\IcecatClient\Services;

use App\ShopifyToolkit\Models\Product as ShopifyProductModel;

class Product extends Base
{
    public function get($gtin)
    {
        $response = $this->client->get('', ['GTIN' => $gtin]);

        return $this->translateAndUnserializeModel($response['data'], ShopifyProductModel::class);
    }

    public function translateAndUnserializeModel(array $data, $className)
    {
        $variants = [];
        $images = [
            [
                'src' => $data['Image']['HighPic'],
            ]
        ];

        $preparedData = [
            'title' => $data['GeneralInfo']['Title'],
            'body_html' => $data['GeneralInfo']['Description']['LongDesc'],
            'vendor' => $data['GeneralInfo']['Brand'],
            'product_type' => $data['GeneralInfo']['Category']['Name']['Value'],
            'variants' => $variants,
            'images' => $images,
        ];

        return $this->unserializeModel($preparedData, $className);
    }
}
