<?php

namespace App\IcecatClient\Services;

use App\ShopifyToolkit\Contracts\Serializeable;

abstract class Base
{
    protected $client;

    protected $nameMap = [];

    protected $unserializationExceptions = [];

    protected $serializationExceptions = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function serializeModel(Serializeable $entity)
    {
        $arr = [];
        $class = new \ReflectionClass($entity);

        $properties = array_map(function ($property) {
            return $property->name;
        }, $class->getProperties());

        foreach ($properties as $property) {
            $value = $entity->{'get'.ucfirst($property)}();

            if (isset($this->serializationExceptions[$property])) {
                $value = $this->{$this->serializationExceptions[$property]}($value);
            }

            $propertyName = $this->getArrayPropertyName($property);

            $arr[$propertyName] = $value;
        }

        return array_filter($arr, function ($a) {
            return $a !== null;
        });
    }

    public function unserializeModel(array $data, $className)
    {
        $class = new \ReflectionClass($className);

        $instance = $class->newInstance();

        foreach ($data as $property => $value) {
            try {
                if (isset($this->unserializationExceptions[$property])) {
                    $value = $this->{$this->unserializationExceptions[$property]}($value);
                }

                $propertyName = $this->getJsonPropertyName($property);
                $property = $class->getProperty($propertyName);
                $property->setAccessible(true);
                $property->setValue($instance, $value);
            } catch (\ReflectionException $e) {
                //dump($e);
            }
        }

        return $instance;
    }

    private function getArrayPropertyName($property)
    {
        $nameMapResult = array_search($property, $this->nameMap);
        if ($nameMapResult) {
            return $nameMapResult;
        }

        return ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $property)), '_');
    }

    private function getJsonPropertyName($property)
    {
        if (isset($this->nameMap[$property])) {
            return $this->nameMap[$property];
        }

        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $property))));
    }
}
