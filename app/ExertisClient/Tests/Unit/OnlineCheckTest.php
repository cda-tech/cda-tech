<?php

namespace App\ExertisClient\Tests\Unit;

use App\ExertisClient\Services\OnlineCheck as OnlineCheckService;
use App\ExertisClient\Tests\Factories\OnlineCheck;

class OnlineCheckTest extends UnitTestCase
{
    /**
     * @var OnlineCheckService
     */
    protected $onlineCheckService;

    public function setUp()
    {
        parent::setUp();

        $mockClient = $this->createMockClient(json_decode(OnlineCheck::getMockXMLObject(), true));

        $this->onlineCheckService = new OnlineCheckService($mockClient);
    }

    public function testOnlineCheckXMLConvertsToModel()
    {
        $onlineCheckModel = $this->onlineCheckService->post(null);

        $this->assertEquals("31-05-2018 4:05:25", $onlineCheckModel->getIssueDateTime());
        $this->assertEquals("Full", $onlineCheckModel->getType());
        $this->assertEquals("CE285A", $onlineCheckModel->getProductCode());
        $this->assertEquals("HP P1102 BLACK TONER CARTRIDGE", $onlineCheckModel->getDescription());
        $this->assertEquals("148", $onlineCheckModel->getAvailability());
        $this->assertEquals("45.86", $onlineCheckModel->getUnitPrice());
        $this->assertEquals("GBP", $onlineCheckModel->getCurrency());
        $this->assertEquals("EAN", $onlineCheckModel->getEAN());

    }
}
