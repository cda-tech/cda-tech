<?php

namespace App\ExertisClient\Tests\Factories;

class OnlineCheck
{
    public static function getMockXMLObject()
    {
        return "{
          \"OnlineCheck\": {
            \"Responsedetails\": {
              \"IssueDateTime\": \"31-05-2018 4:05:25\",
              \"Type\": \"Full\",
              \"Productcode\": \"CE285A\",
              \"Description\": \"HP P1102 BLACK TONER CARTRIDGE\",
              \"Availability\": \"148\",
              \"UnitPrice\": \"45.86\",
              \"Currency\": \"GBP\",
              \"EAN\": \"EAN\"
            }
          }
        }";
    }
}
