<?php

namespace App\ExertisClient\Tests;

use App\ExertisClient\Services\Client as ClientService;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function createMockClient($array)
    {
        $mockClient = $this->createMock(ClientService::class);
        $mockClient->expects($this->any())->method('post')->willReturn($array);
        $mockClient->expects($this->any())->method('get')->willReturn($array);

        return $mockClient;
    }
}
