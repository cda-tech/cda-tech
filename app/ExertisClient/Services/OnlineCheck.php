<?php

namespace App\ExertisClient\Services;

use App\ExertisClient\Models\OnlineCheck as OnlineCheckModel;
use SimpleXMLElement;

class OnlineCheck extends Base
{
    public function post($body)
    {
        $response = $this->sendXmlOverPost('http://195.171.88.165/', $body);

        return $this->unserializeModel($response, OnlineCheckModel::class);
    }

    private function sendXmlOverPost($url, $xml) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, ["Content-Type: text/xml"]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);

        $xml = new SimpleXMLElement($result);
        $result = $this->client->xmlObjToArr($xml);

        return $result;
    }
}
