<?php

namespace App\ExertisClient\Services;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use SimpleXMLElement;

class Client
{
    protected $rootPath = 'http://195.171.88.165/';

    protected $guzzleClient;

    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    public function post($path, $params = [], $body)
    {
        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $headers = ['Content-Type' => 'text/xml'];

        $request = new Request('POST', $uri, $headers, $body);

        return $this->sendRequestToExertis($request);
    }

    public function put($path, $params = [], $body)
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('PUT', $uri, $headers, $body);

        return $this->sendRequestToExertis($request);
    }

    public function delete($path, $params = [])
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('DELETE', $uri, $headers);

        return $this->sendRequestToExertis($request);
    }

    public function get($path = '', $params = [])
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('GET', $uri, $headers);

        return $this->sendRequestToExertis($request);
    }

    public function head($path, $params = [])
    {
        $headers = [];

        $uri = new Uri($this->rootPath . $path);
        $uri = $uri->withQuery(http_build_query($params));

        $request = new Request('HEAD', $uri, $headers);

        return $this->sendRequestToExertis($request);
    }

    private function sendRequestToExertis(Request $request)
    {
        $result = null;

        $response = $this->guzzleClient->send($request);
        $xml = new SimpleXMLElement($response->getBody()->getContents());
        $result = $this->xmlObjToArr($xml);

        return $result;
    }

    public function xmlObjToArr(SimpleXMLElement $obj)
    {
        $array = [];

        foreach ($obj as $key => $value) {
            if ($value->children()->count() > 0) {
                $array[$key] = $this->xmlObjToArr($value);
            } else {
                $array[$key] = (string) $value;
            }
        }

        return $array;
    }
}
