<?php

namespace App\ExertisClient\Services;

use App\ExertisClient\Models\ISerializable;
use ReflectionClass;
use ReflectionException;

abstract class Base
{
    protected $client;

    protected $unserializationExceptions = [];

    protected $serializationExceptions = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function serializeModel(ISerializable $entity)
    {

    }

    public function unserializeModel(array $data, $className, $instance = null)
    {
        $class = new ReflectionClass($className);
        if ($instance === null) {
            $instance = $class->newInstance();
        }

        $flatData = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($data));

        foreach ($flatData as $property => $value) {
            try {
                if (isset($this->unserializeExceptions[$property])) {
                    $value = $this->{$this->unserializationExceptions[$property]}($data);
                }

                if ($class->hasProperty($property)) {
                    $modelProperty = $class->getProperty($property);
                    $modelProperty->setAccessible(true);
                    $modelProperty->setValue($instance, $value);
                }
            } catch (ReflectionException $e) {
                throw new ReflectionException("Could not unserialize property: " . $value, $e->getCode());
            }
        }

        return $instance;
    }
}
