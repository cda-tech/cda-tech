<?php

namespace App\ExertisClient\Models;

abstract class Base implements ISerializable
{
    public function hasPropertyWithoutCase($property)
    {
        $variables = get_class_vars(get_class($this));

        foreach ($variables as $variable) {
            if (strtolower($variable) === strtolower($property)) {
                return true;
            }
        }

        return false;
    }

    public function toArray()
    {
        $model = [];

        $classPath = get_class($this);
        $className = substr($classPath, strrpos($classPath, '\\') + 1);

        $model[$className] = get_class_vars($classPath);

        return $model;
    }
}
