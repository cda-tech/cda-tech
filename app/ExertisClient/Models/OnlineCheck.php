<?php

namespace App\ExertisClient\Models;

/**
 * Class OnlineCheck
 */
class OnlineCheck extends Base
{
    /**
     * @var string
     */
    protected $IssueDateTime;

    /**
     * @var string
     */
    protected $Type;

    /**
     * @var string
     */
    protected $Productcode;

    /**
     * @var string
     */
    protected $Description;

    /**
     * @var int
     */
    protected $Availability;

    /**
     * @var float
     */
    protected $UnitPrice;

    /**
     * @var string
     */
    protected $Currency;

    /**
     * @var int
     */
    protected $EAN;

    /**
     * @return string
     */
    public function getIssueDateTime()
    {
        return $this->IssueDateTime;
    }

    /**
     * @param string $issueDateTime
     */
    public function setIssueDateTime($issueDateTime)
    {
        $this->IssueDateTime = $issueDateTime;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->Type = $type;
    }

    /**
     * @return string
     */
    public function getProductcode()
    {
        return $this->Productcode;
    }

    /**
     * @param string $productcode
     */
    public function setProductcode($productcode)
    {
        $this->Productcode = $productcode;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->Description = $description;
    }

    /**
     * @return int
     */
    public function getAvailability()
    {
        return $this->Availability;
    }

    /**
     * @param int $availability
     */
    public function setAvailability($availability)
    {
        $this->Availability = $availability;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->UnitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->UnitPrice = $unitPrice;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->Currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->Currency = $currency;
    }

    /**
     * @return int
     */
    public function getEAN()
    {
        return $this->EAN;
    }

    /**
     * @param int $ean
     */
    public function setEAN($ean)
    {
        $this->EAN = $ean;
    }
}
