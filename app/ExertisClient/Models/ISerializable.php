<?php

namespace App\ExertisClient\Models;

interface ISerializable
{
    public function hasPropertyWithoutCase($property);
}
