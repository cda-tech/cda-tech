<?php

namespace App\Services;

interface IRestfulService
{
    public function create($data);

    public function edit($data);

    public function delete($id);

    public function get($id);
}
