<?php

namespace App\Services;

use App\ExertisClient\Services\OnlineCheck;
use App\IcecatClient\Services\Product as IcecatProductService;
use App\Services\IProduct as IProductService;
use App\ShopifyToolkit\Services\Product as ShopifyProductService;
use App\ShopifyToolkit\Services\Variant as ShopifyVariantService;
use App\ShopifyToolkit\Services\Client as ShopifyClient;
use Mockery\Exception;

class Product implements IProductService
{
    private $onlineCheckService;

    private $icecatProductService;

    private $shopifyProductService;

    private $shopifyVariantService;

    private $shopifyClient;

    public function __construct(OnlineCheck $onlineCheckService,
                                IcecatProductService $icecatProductService,
                                ShopifyProductService $shopifyProductService,
                                ShopifyVariantService $shopifyVariantService,
                                ShopifyClient $shopifyClient)
    {
        $this->onlineCheckService = $onlineCheckService;
        $this->icecatProductService = $icecatProductService;
        $this->shopifyProductService = $shopifyProductService;
        $this->shopifyVariantService = $shopifyVariantService;
        $this->shopifyClient = $shopifyClient;
    }

    public function create($productData)
    {
        $productModel = $this->icecatProductService->get($productData['EAN']);

        return $this->shopifyProductService->create($productModel);
    }

    public function edit($productData)
    {
        throw new Exception('Not Implemented', 501);
    }

    public function delete($id)
    {
        throw new Exception('Not Implemented', 501);
    }

    public function get($id)
    {
        throw new Exception('Not Implemented', 501);
    }

    public function getLiveStockAndPrice($body)
    {
        $variantId = $body['VariantId'];

        $productCode = $body['Productcode'];
        $productCodeType = $body['ProductCodeType'];

        $bodyString = "<OnlineCheck><Requestdetails><Username>CLI101</Username><AuthCode>P67PTlyl5i</AuthCode><Type>Full</Type><Productcode>$productCode</Productcode><ProductCodeType>$productCodeType</ProductCodeType></Requestdetails></OnlineCheck>";

        $stockAndPrice = $this->onlineCheckService->post($bodyString);

        $price = $stockAndPrice->getUnitPrice();
        if ($stockAndPrice !== null && $price > 0) {
            $markedUpPrice = $price * 1.2;
            $markedUpPrice = $price < 5.00 ? $markedUpPrice + 0.50 : $markedUpPrice;
            
            $stockAndPrice->setUnitPrice($markedUpPrice);

            $variantPriceData = [
                'variant' => [
                    'id' => $variantId,
                    'price' => $markedUpPrice,
                ],
            ];

            $variant = $this->shopifyClient->put("admin/variants/{$variantId}.json", [], $variantPriceData);

            if ($variant) {
                $stockData = [
                    'location_id' => 6405914736,
                    'inventory_item_id' => $variant['variant']['inventory_item_id'],
                    'available' => $stockAndPrice->getAvailability(),
                ];

                $this->shopifyClient->post("admin/inventory_levels/set.json", [], $stockData);
            }
        }

        return $stockAndPrice;
    }
}
