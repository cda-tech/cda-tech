<?php

namespace App\Services;

use App\Services\IIcecat as IIcecatService;
use App\IcecatClient\Services\Product as IcecatProductService;
use Mockery\Exception;

class Icecat implements IIcecatService
{
    protected $icecatProductService;

    public function __construct(IcecatProductService $icecatProductService)
    {
        $this->icecatProductService = $icecatProductService;
    }

    public function create($productData)
    {
        throw new Exception('Not Implemented', 501);
    }

    public function edit($productData)
    {
        throw new Exception('Not Implemented', 501);
    }

    public function delete($id)
    {
        throw new Exception('Not Implemented', 501);
    }

    public function get($id)
    {
        return $this->icecatProductService->get($id);
    }
}
