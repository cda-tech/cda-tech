<?php

namespace App\ShopifyToolkit\Contracts;

/**
 * Interface ShopAccessInfo.
 */
interface ShopAccessInfo
{
    /**
     * @return string
     */
    public function getToken();
}
