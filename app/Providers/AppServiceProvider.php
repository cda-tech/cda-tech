<?php

namespace App\Providers;

use App\Services\IIcecat as IIcecatService;
use App\Services\Icecat as IcecatService;
use App\Services\IProduct as IProductService;
use App\Services\Product as ProductService;
use App\ShopifyToolkit\Contracts\ApiSleeper;
use App\ShopifyToolkit\Contracts\RequestHookInterface;
use App\ShopifyToolkit\Contracts\ShopAccessInfo;
use App\ShopifyToolkit\Contracts\ShopBaseInfo;
use App\ShopifyToolkit\Models\Shop;
use App\ShopifyToolkit\Services\Client;
use App\ShopifyToolkit\Support\ShopifyApiHandler;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerInternalServices();
        $this->registerShopifyToolkitServices();
    }

    public function registerInternalServices()
    {
        $this->app->singleton(IProductService::class, function ($app) { return $app->make(ProductService::class); });
        $this->app->singleton(IIcecatService::class, function ($app) { return $app->make(IcecatService::class); });
    }
    
    public function registerShopifyToolkitServices()
    {
        $shop = app(Shop::class);
        $shop->setDomain('clickoffice-supplies.myshopify.com');
        $shop->setMyshopifyDomain('clickoffice-supplies.myshopify.com');
        $shop->setToken('ddfef0fd12299fb3d47545fd872e1d27:d0a8b9398f9fff3fa8cd54b45409e2f8');

        $this->app->instance(ShopBaseInfo::class, $shop);

        $this->app->bind(ApiSleeper::class, ShopifyApiHandler::class);
        $this->app->instance(ShopBaseInfo::class, $shop);
        $this->app->instance(ShopAccessInfo::class, $shop);
    }
}
