<?php

namespace App\Http\Middleware;

use App\ShopifyToolkit\Contracts\ShopBaseInfo;
use App\ShopifyToolkit\Models\Shop;
use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class BindShop
{
    protected $app;

    protected $except = [];

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function handle($request, Closure $next)
    {
        $shopUrl = $this->getShopUrl($request);

        if ($shopUrl === null) {
            throw new \Exception('Shop not found.', 404);
        }

        $shop = app(Shop::class);
        $shop->setMyshopifyDomain($shopUrl);

        $this->app->instance(ShopBaseInfo::class, $shop);

        // Setup access token

        return $next($request);
    }

    private function getShopUrl(Request $request)
    {
        return config(['app.shop_url']) !== null
            ? config(['app.shop_url'])
            : $request->shop_url;
    }
}