<?php

namespace App\Http\Controllers;

use App\Services\IIcecat as IIcecatService;

class IcecatController extends Controller
{
    public function __construct(IIcecatService $icecatService)
    {
        parent::__construct($icecatService);
    }
}
