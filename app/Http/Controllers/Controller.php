<?php

namespace App\Http\Controllers;

use App\Http\Middleware\BindShop;
use App\Services\IRestfulService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $service;

    public function __construct(IRestfulService $restfulService)
    {
//        $bindShop = app(BindShop::class);

//        $this->middleware(function($request, $next) use ($bindShop) {
//            return $bindShop->handle($request, $next);
//        });

        $this->service = $restfulService;
    }

    public function create(Request $request)
    {
        return $this->handleRequest(201, function() use ($request)
        {
            $data = json_decode($request->getContent(), true);

            $response = [];
            $response['success'] = true;
            $response['content'] = $this->service->create($data);

            return $response;
        });
    }

    public function edit(Request $request)
    {
        return $this->handleRequest(200, function() use ($request)
        {
            $id = $request->id;
            $data = json_decode($request->getContent(), true);

            $data['student_id'] = $id;

            $response = [];
            $response['success'] = true;
            $response['content'] = $this->service->edit($data);

            return $response;
        });
    }

    public function delete(Request $request)
    {
        return $this->handleRequest(200, function() use ($request)
        {
            $id = $request->id;

            $response = [];
            $response['success'] = true;
            $response['content'] = $this->service->delete($id);

            return $response;
        });
    }

    public function get(Request $request)
    {
        return $this->handleRequest(200, function() use ($request)
        {
            $id = $request->id;

            $response = [];
            $response['success'] = true;
            $response['content'] = $this->service->get($id);

            return $response;
        });
    }

    protected function handleRequest($successStatusCode, $func)
    {
        $statusCode = $successStatusCode;
        $response = [];

        try {
            $response = $func();
        } catch (\Exception $e) {
            $statusCode = $e->getCode() === 0 ? 500 : $e->getCode();
            $response['success'] = false;
            $response['content'] = $e->getMessage();
        }

        return new JsonResponse($response, $statusCode);
    }
}
