<?php

namespace App\Http\Controllers;

use App\ExertisClient\Models\OnlineCheck;
use App\Services\IProduct as IProductService;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    public function __construct(IProductService $productService)
    {
        parent::__construct($productService);
    }

    public function create(Request $request)
    {
        return $this->handleRequest(201, function() use ($request)
        {
            $body = json_decode($request->getContent(), true);

            if (!isset($body['EAN'])) {
                throw new \Exception('EAN not provided.', 400);
            }

            $response = [];
            $response['success'] = true;
            $response['content'] = $this->service->create($body);
        });
    }

    public function getLiveStockAndPrice(Request $request)
    {
        return $this->handleRequest(200, function() use ($request)
        {
            $body = json_decode($request->getContent(), true);

            $onlineCheck = $this->service->getLiveStockAndPrice($body);

            $response = [];
            $response['success'] = true;
            $response['content'] = $this->mapToArray($onlineCheck);

            return $response;
        });
    }

    private function mapToArray(OnlineCheck $onlineCheck)
    {
        return [
            'issue_date_time' => $onlineCheck->getIssueDateTime(),
            'type' => $onlineCheck->getType(),
            'product_code' => $onlineCheck->getProductcode(),
            'description' => $onlineCheck->getDescription(),
            'availability' => $onlineCheck->getAvailability(),
            'unit_price' => $onlineCheck->getUnitPrice(),
            'currency' => $onlineCheck->getCurrency(),
            'ean' => $onlineCheck->getEAN(),
        ];
    }
}
