<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('{shop_url}')->group(function() {
    Route::get('icecat/products/{id}', ['as' => 'product', 'uses' => 'IcecatController@get']);

    Route::post('product', ['as' => 'product', 'uses' => 'ProductController@create']);
    Route::post('product/stockAndPrice', ['as' => 'product', 'uses' => 'ProductController@getLiveStockAndPrice']);
});
