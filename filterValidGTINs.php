#!/usr/bin/php
<?php

chdir(dirname(__FILE__));

if (!isset($argv[1])) {
    die('Please pass in a filename for GTIN list.');
}

$output = `php ./app/Batch/filterValidGTINs.php $argv[1]`;
echo $output;
